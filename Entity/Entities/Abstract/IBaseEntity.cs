namespace WebApplication4.Entity.Entities.Abstract
{
    public interface IBaseEntity<TKey>
    {
        TKey Id { get; }
    }
}