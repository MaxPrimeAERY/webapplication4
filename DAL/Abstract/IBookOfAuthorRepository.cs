using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication4.Entity.Entities;

namespace WebApplication4.DAL.Abstract
{
    public interface IBookOfAuthorRepository : IBaseRepository<int, BookOfAuthor>
    {
        Task<IList<BookOfAuthor>> GetByAuthorId(int fullId);
        
        Task<IList<BookOfAuthor>> GetByBookId(int fullId);
    }
}