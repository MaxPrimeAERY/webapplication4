using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication4.Entity.Entities;

namespace WebApplication4.DAL.Abstract
{
    public interface IAuthorRepository  : IBaseRepository<int, Author>
    {
        
    }
}