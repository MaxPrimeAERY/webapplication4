using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication4.Entity.Entities;

namespace WebApplication4.DAL.Abstract
{
    public interface IBooksRepository : IBaseRepository<int, Books>
    {
        Task<IList<Books>> GetBySectionsId(int sectId);
    }
}