using WebApplication4.Entity.Entities;

namespace WebApplication4.DAL.Abstract
{
    public interface ISectionsRepository : IBaseRepository<int, Sections>
    {
        
    }
}