using System.Data.Common;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;
using WebApplication4.DAL.Pgsql.Repository;
using WebApplication4.DAL.Abstract;
using WebApplication4.DAL.EntityFramework;

namespace WebApplication4.DAL.Pgsql
{
    public static class Configuration
    {
//        public static IServiceCollection UsePgSqlAdoRepositories(this IServiceCollection services,
//            string connectionString)
//        {
//            //we ned concrete implementation for transaction manager!
//            services.AddScoped(ctx => new NpgsqlConnection(connectionString));
//            services.AddScoped<DbConnection>(ctx => ctx.GetService<NpgsqlConnection>());
//
//            services.AddScoped<ITransactionManager, PgSqlTransactionManager>();
//
//            //services.AddScoped<IBooksRepository, EFBooksRepository>();
//            services.AddDbContext<DbWebAppContext>
//            (options =>
//                options.UseSqlServer(new NpgsqlConnection(connectionString)));
//            return services;
//        }
        
        public static IServiceCollection UsePgSqlEF(this IServiceCollection services, string connectionString)
        {
            services.AddScoped<IAuthorRepository,  AuthorRepository>();
            services.AddScoped<IBookOfAuthorRepository,  BookOfAuthorRepository>();
            services.AddScoped<IBooksRepository,  BooksRepository>();
            services.AddScoped<ICardOfReaderRepository,  CardOfReaderRepository>();
            services.AddScoped<IReaderRepository,  ReaderRepository>();
            services.AddScoped<ISectionsRepository,  SectionsRepository>();

            services.AddDbContext<AppContext>(opt => opt.UseNpgsql(connectionString));
            
            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<AppContext>()
                .AddDefaultTokenProviders();
            
            

            return services;
        }
    }
}