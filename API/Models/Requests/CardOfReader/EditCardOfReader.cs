using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication4.API.Models.Requests.CardOfReader
{
    public class EditCardOfReader
    {
        [Required]
        public DateTime? Give_Time { get; set; }
    }
}