using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication4.API.Models.Requests.BookOfAuthor;
using WebApplication4.DAL.Abstract;
using WebApplication4.Entity.Entities;

namespace WebApplication4.Services.Impl
{
    public class BookOfAuthorService : IBookOfAuthorService
    {
        private IBookOfAuthorRepository _bookOfAuthorRepo;

        public BookOfAuthorService(IBookOfAuthorRepository bookOfAuthorRepo)
        {
            _bookOfAuthorRepo = bookOfAuthorRepo;

        }

        public Task<BookOfAuthor> GetById(int id)
        {
            return _bookOfAuthorRepo.GetById(id);
        }

        public Task<IList<BookOfAuthor>> GetAll()
        {
            return _bookOfAuthorRepo.GetAll();
        }
        
        public Task<IList<BookOfAuthor>> GetByAuthorId(int id)
        {
            return _bookOfAuthorRepo.GetByAuthorId(id);
        }
        
        public Task<IList<BookOfAuthor>> GetByBookId(int id)
        {
            return _bookOfAuthorRepo.GetByBookId(id);
        }
        
        public async Task Delete(int id)
        {
            var res = await _bookOfAuthorRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }

        public  async Task<BookOfAuthor> Create(CreateBookOfAuthor value)
        {
            var bookofAuthor = new BookOfAuthor()
            {
                AuthorId = value.AuthorId,
                BookId = value.BookId
            };

            var id = await _bookOfAuthorRepo.Insert(bookofAuthor);
            bookofAuthor.Id = id;
            return bookofAuthor;
        }
        
        public async Task Update(int bookofAuthorId, int? valueAuthorId, int? valueBookId)
        {
            var bookofAuthor = await _bookOfAuthorRepo.GetById(bookofAuthorId);
            bookofAuthor.AuthorId = valueAuthorId;
            bookofAuthor.BookId = valueBookId;
            await _bookOfAuthorRepo.Update(bookofAuthor);
        }

    }
}