using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication4.API.Models.Requests.CardOfReader;
using WebApplication4.Entity.Entities;

namespace WebApplication4.Services
{
    public interface ICardOfReaderService
    {
        Task<CardOfReader> GetById(int id);
        Task<IList<CardOfReader>> GetAll();
        Task<IList<CardOfReader>> GetByReaderId(int id);
        Task<IList<CardOfReader>> GetByBookId(int id);
        Task Delete(int id);
        Task Update(int corId, DateTime? valueGiveTimeOffset);
        Task<CardOfReader> Create(CreateCardOfReader value);
    }
}