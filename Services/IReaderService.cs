using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication4.API.Models.Requests.Reader;
using WebApplication4.Entity.Entities;

namespace WebApplication4.Services
{
    public interface IReaderService
    {
        Task<Reader> GetById(int id);
        Task<IList<Reader>> GetAll();
        Task Delete(int id);
        Task Update(int readerId, string valueText);
        Task<Reader> Create(CreateReader value);
    }
}