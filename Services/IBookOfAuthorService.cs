using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication4.API.Models.Requests.BookOfAuthor;
using WebApplication4.Entity.Entities;

namespace WebApplication4.Services
{
    public interface IBookOfAuthorService
    {
        Task<BookOfAuthor> GetById(int id);
        Task<IList<BookOfAuthor>> GetAll();
        Task<IList<BookOfAuthor>> GetByAuthorId(int id);
        Task<IList<BookOfAuthor>> GetByBookId(int id);
        Task Delete(int id);
        Task Update(int bookOfAuthorId, int? valueBooksId, int? valueBookId);
        Task<BookOfAuthor> Create(CreateBookOfAuthor value);
    }
}