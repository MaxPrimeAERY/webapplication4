using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication4.API.Models.Requests.Author;
using WebApplication4.DAL.EntityFramework;
using WebApplication4.Entity.Entities;
using WebApplication4.MvcExt;
using WebApplication4.Services;

namespace WebApplication4.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _AuthorSvc;

        
        public AuthorController(IAuthorService AuthorSvc)
        {
            _AuthorSvc = AuthorSvc;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IList<Author>>> Get()
        {
            return new ActionResult<IList<Author>>(await _AuthorSvc.GetAll());
        }
        
        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Author>> Get(int id)
        {
            var Author = await _AuthorSvc.GetById(id);
            if (Author == null)
            {
                return new NotFoundObjectResult(null);
            }

            return Author;
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<Author>> Post([FromBody] CreateAuthor value)
        {
            return await _AuthorSvc.Create(value);
        }

        // PUT api/Author/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Author>> Put(int id, [FromBody] EditAuthor value)
        {
            
            await _AuthorSvc.Update(id, value.Name);
            return await _AuthorSvc.GetById(id);
        }

        // DELETE api/values/5
        
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _AuthorSvc.Delete(id);
        }
    }
}